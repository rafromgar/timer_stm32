`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.12.2018 20:34:23
// Design Name: 
// Module Name: one
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module timer #(parameter W=32, R=20)(input clk, input rst, input cyc_m2s,
        input [W-1:0] data_m2s, input [W-1:0] addr_m2s, input we_m2s,
        output reg [W-1:0] data_s2m, output ack_s2m, output err_s2m, 
        output [1:0] s, output [1:0] s_next, output ack_next, output reg clk_psc);
        
        parameter BASE_ADDR = 32'h40010000;
        
        parameter TIM1_CR1_ADDR = BASE_ADDR + 32'h00;
        parameter TIM1_CR2_ADDR = BASE_ADDR + 32'h04;
        parameter TIM1_SMCR_ADDR = BASE_ADDR + 32'h08;
        parameter TIM1_DIER_ADDR = BASE_ADDR + 32'h0C;
        parameter TIM1_SR_ADDR = BASE_ADDR + 32'h10;
        parameter TIM1_EGR_ADDR = BASE_ADDR + 32'h14;
        parameter TIM1_CCMR1_ADDR = BASE_ADDR + 32'h18;
        parameter TIM1_CCMR2_ADDR = BASE_ADDR + 32'h1C;
        parameter TIM1_CCER_ADDR = BASE_ADDR + 32'h20;
        parameter TIM1_CNT_ADDR = BASE_ADDR + 32'h24;
        parameter TIM1_PSC_ADDR = BASE_ADDR + 32'h28;
        parameter TIM1_ARR_ADDR = BASE_ADDR + 32'h2C;
        parameter TIM1_RCR_ADDR = BASE_ADDR + 32'h30;
        parameter TIM1_CCR1_ADDR = BASE_ADDR + 32'h34;
        parameter TIM1_CCR2_ADDR = BASE_ADDR + 32'h38;
        parameter TIM1_CCR3_ADDR = BASE_ADDR + 32'h3C;
        parameter TIM1_CCR4_ADDR = BASE_ADDR + 32'h40;
        parameter TIM1_BTDR_ADDR = BASE_ADDR + 32'h44;
        parameter TIM1_DCR_ADDR = BASE_ADDR + 32'h48;
        parameter TIM1_DMAR_ADDR = BASE_ADDR + 32'h4C;
        
        localparam IDLE = 2'b00, READ = 2'b01, WRITE = 2'b10;
        
        reg [1:0] ss, ss_next;
        
        reg we_comb, re_comb, err_next;
        integer index_comb;
        
        reg ack_r, ack_r_next;
        reg err_r;
        
        //Prescaler aux variables
        //reg clk_psc; Output so that I can see it in the testbench
        reg [W-1:0] aux, psc;
        
        reg [W-1:0] timer_registers [R-1:0];
        // reg[0] = TIM1_CR1
        // reg[1] = TIM1_CR2
        // reg[2] = TIM1_SMCR
        // reg[3] = TIM1_DIER
        // reg[4] = TIM1_SR
        // reg[5] = TIM1_EGR
        // reg[6] = TIM1_CCMR1
        // reg[7] = TIM1_CCMR2
        // reg[8] = TIM1_CCER
        // reg[9] = TIM1_CNT
        // reg[10] = TIM1_PSC
        // reg[11] = TIM1_ARR
        // reg[12] = TIM1_RCR
        // reg[13] = TIM1_CCR1
        // reg[14] = TIM1_CCR2
        // reg[15] = TIM1_CCR3
        // reg[16] = TIM1_CCR4
        // reg[17] = TIM1_BTDR
        // reg[18] = TIM1_DCR
        // reg[19] = TIM1_DMAR
        
        //reg [W-1:0] TIM1_CR1;
        
        
        assign ack_s2m = ack_r;
        assign err_s2m = err_r;
        assign s = ss;
        assign s_next = ss_next;
        assign ack_next = ack_r_next;
        
        always @(posedge clk)
        begin
            if(rst == 1)
            begin
                ss <= IDLE;
                
                timer_registers[0] <= 0;
                timer_registers[1] <= 0;
                timer_registers[2] <= 0;
                timer_registers[3] <= 0;
                timer_registers[4] <= 0;
                timer_registers[5] <= 0;
                timer_registers[6] <= 0;
                timer_registers[7] <= 0;
                timer_registers[8] <= 0;
                timer_registers[9] <= 0;
                timer_registers[10] <= 0;
                timer_registers[11] <= 0;
                timer_registers[12] <= 0;
                timer_registers[13] <= 0;
                timer_registers[14] <= 0;
                timer_registers[15] <= 0;
                timer_registers[16] <= 0;
                timer_registers[17] <= 0;
                timer_registers[18] <= 0;
                timer_registers[19] <= 0;
                
                //TIM1_CR1 <= 0;
                
                data_s2m <= 0;
                ack_r <= 0;
                err_r <= 0;
                
                we_comb <= 0;
                re_comb <= 0;
                err_next <= 0;
                index_comb <= 0;
                
                aux <= 0;
                psc <= 0;
                clk_psc <= 0;
                
            end
            else
            begin
                if (we_comb == 1)
                begin
                    //TIM1_CR1 <= data_m2s;
                                  
                    timer_registers[index_comb - 1] <= data_m2s;
                    err_r <= 0;
                    we_comb <= 0;
                end
                else if (re_comb == 1)
                begin
                    //data_s2m <= TIM1_CR1;

                    data_s2m <= timer_registers[index_comb - 1];
                    err_r <= 0;
                    re_comb <= 0;
                end
                //else if (err_next)  err_r <= 1;
                err_r <= err_next;

                ss <= ss_next;
                ack_r <= ack_r_next;

            end
        end
       
        always@(posedge clk)
        begin
            if (rst == 0)
            begin
                psc <= timer_registers[10];
                aux <= aux + 1;
                if (aux == psc)
                begin
                    aux <= 0;
                    clk_psc <= !clk_psc;
                end
            end
        end
        
        always@(*)
        begin          
            ack_r_next = 0;
            err_next = 0;

            if (cyc_m2s)
            begin
                if (we_m2s)         we_comb = 1;    
                else                re_comb = 1;
            
                case (addr_m2s)             
                    TIM1_CR1_ADDR:      index_comb = 1; 
                    TIM1_CR2_ADDR:      index_comb = 2;
                    TIM1_SMCR_ADDR:     index_comb = 3;
                    TIM1_DIER_ADDR:     index_comb = 4;
                    TIM1_SR_ADDR:       index_comb = 5;
                    TIM1_EGR_ADDR:      index_comb = 6;
                    TIM1_CCMR1_ADDR:    index_comb = 7;
                    TIM1_CCMR2_ADDR:    index_comb = 8;
                    TIM1_CCER_ADDR:     index_comb = 9;
                    TIM1_CNT_ADDR:      index_comb = 10;
                    TIM1_PSC_ADDR:      index_comb = 11;
                    TIM1_ARR_ADDR:      index_comb = 12;
                    TIM1_RCR_ADDR:      index_comb = 13;
                    TIM1_CCR1_ADDR:     index_comb = 14;
                    TIM1_CCR2_ADDR:     index_comb = 15;
                    TIM1_CCR3_ADDR:     index_comb = 16;
                    TIM1_CCR4_ADDR:     index_comb = 17;
                    TIM1_BTDR_ADDR:     index_comb = 18;
                    TIM1_DCR_ADDR:      index_comb = 19;
                    TIM1_DMAR_ADDR:     index_comb = 20;
                    default:
                    begin            
                        re_comb = 0;
                        we_comb = 0;
                        index_comb = 0;
                        err_next = 1;
                    end
                endcase
                        
                if (index_comb != 0 ) ack_r_next = 1;

            end

        end
    
endmodule
