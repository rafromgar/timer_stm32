`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.12.2018 18:00:59
// Design Name: 
// Module Name: tb1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb1();

parameter W = 32;

reg clk, rst;
reg cyc_m2s, we_m2s;
reg [W-1:0] data_m2s;
reg [W-1:0] addr_m2s;

wire [W-1:0] data_s2m;
wire ack_s2m;
wire err_s2m;

wire [1:0] s, s_next;
wire ack_next;
wire clk_psc;

always #5 clk = !clk;

timer #(.W(W))t1(.clk(clk), .rst(rst), .cyc_m2s(cyc_m2s), .we_m2s(we_m2s), 
    .addr_m2s(addr_m2s), .data_m2s(data_m2s), .data_s2m(data_s2m),
    .ack_s2m(ack_s2m), .err_s2m(err_s2m), .s(s), .s_next(s_next), .ack_next(ack_next), .clk_psc(clk_psc));
    
initial
begin
    clk <= 0;
    rst = 1;
    cyc_m2s = 0;
    we_m2s = 0;
    data_m2s = 0;
    addr_m2s = 0;
    
    repeat (2) @(posedge clk);
    
    @(posedge clk) rst = 0;
    
    //WRITE 
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
    end
    
    @(posedge clk);
    begin
        addr_m2s = 32'h40010000;
        data_m2s = 32'h11111111;
        cyc_m2s = 0;
        we_m2s = 0;
        
    end
    
    repeat (2) @(posedge clk);
    
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
    end
    
    @(posedge clk);
    begin
        addr_m2s = 32'h40010028;
        data_m2s = 32'h00000002;
        cyc_m2s = 0;
        we_m2s = 0;
        
    end

    //ERROR WRITE
    repeat (2) @(posedge clk);
    
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
    end
    
    @(posedge clk);
    begin
        addr_m2s = 32'h40010007;
        data_m2s = 32'h01010101;
        cyc_m2s = 0;
        we_m2s = 0;
        
    end

    repeat (2) @(posedge clk);
    
    //READ 
    @(posedge clk);
    begin
        cyc_m2s = 1;        
    end
    
    @(posedge clk);
    begin
        cyc_m2s = 0;
        addr_m2s = 32'h40010028;
        
    end
    
    repeat (2) @(posedge clk);   

    @(posedge clk);
    begin
        cyc_m2s = 1;        
    end
    
    @(posedge clk);
    begin
        cyc_m2s = 0;
        addr_m2s = 32'h40010000;
        
    end
   
   //ERROR READ
   repeat (2) @(posedge clk);   
    
    @(posedge clk);
    begin
        cyc_m2s = 1;        
    end
    
    @(posedge clk);
    begin
        cyc_m2s = 0;
        addr_m2s = 32'h40010001;
        
    end
    
    repeat (2) @(posedge clk);  
    
    #20 $finish;    
end
endmodule
